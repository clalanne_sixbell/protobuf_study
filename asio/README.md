# overview

Basic client/server architecture using asio and protobufs, simplest that 
I can think of

# Proto compilation 
## OSX
### C++
```
protoc -I. --cpp_out=. hmp_sixbell.proto
```
### Python
```
protoc -I. --python_out=. hmp_sixbell.proto
```

# Server compilation
Notice boost and protobuf dependencies

## OSX and Ubuntu 18.04
```
clang++ -std=c++17 Connection.cpp hmp_sixbell.pb.cc Server.cpp server.cpp -I. -lprotobuf -lboost_system -o server -Wall
```
### Ubuntu 18.04
 * clang, v6.0.0
 * protobuf, v3.6.1
 * boost, v1.65.1

# Client
## Python
### Ubuntu 18.04
```
sudo pip install google
sudo pip install protobuf
```

## C++
### Compilation
#### Ubuntu 18.04

Interesting enough, I have to specify -lpthread, even though I'm using 
```std::thread```!

```
clang++ -std=c++17 client.cpp hmp_sixbell.pb.cc -I. -lprotobuf -lboost_system -lpthread -o client -Wall
```


