

#ifndef __SERVER_QOFJOQWOEFPJQWFE__
#define __SERVER_QOFJOQWOEFPJQWFE__

#include "Connection.hpp"

#include <boost/asio.hpp>

class Server {
   public:
    Server(boost::asio::io_service& io, const int port);

    void handle_accept(Connection::Pointer connection,
                       const boost::system::error_code& error);

   private:
    void start();

   private:
    boost::asio::ip::tcp::acceptor acceptor_;
};

#endif  //__SERVER_QOFJOQWOEFPJQWFE__
