#ifndef __CONNECTION_AFWEJAWEOJFPOWEJ__
#define __CONNECTION_AFWEJAWEOJFPOWEJ__

#include "hmp_sixbell.pb.h"

#include "packedmessage.h"

#include <boost/asio.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <vector>

class Connection : public boost::enable_shared_from_this<Connection> {
   public:
    typedef boost::shared_ptr<Connection> Pointer;
    typedef boost::shared_ptr<hmp_sixbell::Request> RequestPointer;
    typedef boost::shared_ptr<hmp_sixbell::Response> ResponsePointer;

    static Pointer create(boost::asio::io_service& io);
    boost::asio::ip::tcp::socket& get_socket();
    void start();

   private:
    Connection(boost::asio::io_service& io);
    void start_read_header();
    void handle_read_header(const boost::system::error_code& error);
    void start_read_body(int msg_len);
    void handle_read_body(const boost::system::error_code& error);
    void handle_request();
    ResponsePointer prepare_response(RequestPointer);

   private:
    boost::asio::ip::tcp::socket socket_;
    PackedMessage<hmp_sixbell::Request> m_packed_request;
    //    hmp_sixbell::Request req;
    std::vector<uint8_t> readbuf_;
};

#endif  //__CONNECTION_AFWEJAWEOJFPOWEJ__
