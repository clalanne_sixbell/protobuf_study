
#include "Connection.hpp"

#include <boost/bind.hpp>

#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;
using std::vector;

Connection::Connection(boost::asio::io_service& io) : socket_(io) {
    cout << "[Connection][Connection]" << endl;
}

Connection::Pointer Connection::create(boost::asio::io_service& io) {
    cout << "[Connection][create]" << endl;
    return Pointer(new Connection(io));
}

boost::asio::ip::tcp::socket& Connection::get_socket() {
    cout << "[Connection][get_socket]" << endl;
    return socket_;
}

void Connection::start() {
    cout << "[Connection][start]" << endl;

    start_read_header();
}

void Connection::start_read_header() {
    cout << "[Connection][start_read_header]" << endl;

    readbuf_.resize(HEADER_SIZE);
    boost::asio::async_read(
        socket_, boost::asio::buffer(readbuf_),
        boost::bind(&Connection::handle_read_header, shared_from_this(),
                    boost::asio::placeholders::error()));
}

void Connection::handle_read_header(const boost::system::error_code& error) {
    cout << "[Connection][handle_read_header]" << endl;
    for (int i = 0; i < readbuf_.size(); ++i) {
        printf("[%02X] ", readbuf_[i]);
    }

    if (!error) {
        unsigned msg_len = m_packed_request.decode_header(readbuf_);
        start_read_body(msg_len);
    }
}

void Connection::start_read_body(int msg_len) {
    cout << "[Connection][start_read_body] msg len[" << msg_len
         << "] header size[" << HEADER_SIZE << "] total: ["
         << HEADER_SIZE + msg_len << "]" << endl;

    readbuf_.resize(HEADER_SIZE + msg_len);
    boost::asio::mutable_buffers_1 buf =
        boost::asio::buffer(&readbuf_[HEADER_SIZE], msg_len);

    boost::asio::async_read(
        socket_, buf,
        boost::bind(&Connection::handle_read_body, shared_from_this(),
                    boost::asio::placeholders::error));
}

void Connection::handle_read_body(const boost::system::error_code& error) {
    cout << "[Connection][handle_read_body]" << endl;

    if (!error) {
        handle_request();
        start_read_header();
    } else {
        cout << "[Connection][handle_read_body] errors reading header" << endl;
    }
}

Connection::ResponsePointer Connection::prepare_response(RequestPointer req) {
    cout << "[Connection][prepare_response] [" << req->type() << "]" << endl;

    string value;
    switch (req->type()) {
        case hmp_sixbell::Request::PLAY: {
            cout << "Playing....." << endl;
            break;
        }
        case hmp_sixbell::Request::GET_BEARER: {
            cout << "Get bearer....." << endl;
            break;
        }
        default:
            cout << "Default case....." << endl;

            break;
    }

    ResponsePointer resp(new hmp_sixbell::Response);
    resp->set_type(
        hmp_sixbell::Response_ResponseType::Response_ResponseType_PLAY_DONE);

    return resp;
};

void Connection::handle_request() {
    cout << "[Connection][handle_request]" << endl;
    for (int i = 0; i < readbuf_.size(); ++i) {
        printf("[%02X] ", readbuf_[i]);
    }

    cout << endl;

    cout << "[unpack] buf size[" << readbuf_.size() << "]" << endl;
    for (const auto& element : readbuf_) printf("[%02X] ", element);
    cout << endl;

    cout << "[unpack]size[" << readbuf_.size() - HEADER_SIZE << "]" << endl;
    cout << "[unpack]size[" << readbuf_.size() << "]" << endl;

    for (const auto& element : readbuf_) printf("[%02X] ", element);

    auto req =
        boost::shared_ptr<hmp_sixbell::Request>(new hmp_sixbell::Request());
    bool parsedReqWorked = req->ParseFromArray(&readbuf_[HEADER_SIZE],
                                               readbuf_.size() - HEADER_SIZE);

    if (parsedReqWorked) {
        cout << "[Connection][handle_request] request type: " << req->type()
             << endl;
        // RequestPointer req1(req);
        cout << "[Connection][handle_request] preparing response" << endl;
        ResponsePointer resp = prepare_response(req);
        cout << "[Connection][handle_request] response type [" << resp->type()
             << "]" << endl;

        vector<uint8_t> writebuf;
        PackedMessage<hmp_sixbell::Response> resp_msg(resp);
        resp_msg.pack(writebuf);
        boost::asio::write(socket_, boost::asio::buffer(writebuf));
    }
}

