
#include "Server.hpp"
#include "Connection.hpp"

#include <boost/bind.hpp>

#include <iostream>

using std::cout;
using std::endl;

Server::Server(boost::asio::io_service& io, const int port)
    : acceptor_(io, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(),
                                                   port)) {
    start();
}

void Server::start() {
    cout << "[Server][start]" << endl;

    Connection::Pointer new_connection =
        Connection::create(acceptor_.get_io_service());

    acceptor_.async_accept(
        new_connection->get_socket(),
        boost::bind(&Server::handle_accept, this, new_connection,
                    boost::asio::placeholders::error));
}

void Server::handle_accept(Connection::Pointer connection,
                           const boost::system::error_code& error) {
    // A new client has connected
    if (!error) {
        // Start the connection
        connection->start();

        // Accept another client
        start();
    }
}

