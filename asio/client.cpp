

#include "hmp_sixbell.pb.h"

#include <boost/asio.hpp>

#include <iostream>
#include <string>
#include <thread>
#include <vector>

using std::cerr;
using std::cout;
using std::endl;
using std::string;

const int HEADER_SIZE = 4;

void encode_header(std::vector<uint8_t>& buf, unsigned size) {
    assert(buf.size() >= HEADER_SIZE);
    buf[0] = static_cast<boost::uint8_t>((size >> 24) & 0xFF);
    buf[1] = static_cast<boost::uint8_t>((size >> 16) & 0xFF);
    buf[2] = static_cast<boost::uint8_t>((size >> 8) & 0xFF);
    buf[3] = static_cast<boost::uint8_t>(size & 0xFF);
}

int decode_header(std::array<uint8_t, 128>& buf) {
    int msg_size = 0;
    for (unsigned i = 0; i < HEADER_SIZE; ++i) {
        msg_size = msg_size * 256 + static_cast<unsigned>(buf[i]);
    }
    return msg_size;
}

class HMPClient {
   public:
    HMPClient(const std::string ip, const int port);

    int async_play();
    void wait4Event();
    hmp_sixbell::Response getEvent() const;

   private:
    void on_connect(const boost::system::error_code& error);
    void on_write(const boost::system::error_code& error);
    void on_read(const boost::system::error_code& error, size_t bytesReceived);

   private:
    const int port_;
    const std::string ip_;
    boost::asio::io_service service_;
    boost::asio::ip::tcp::socket socket_;

    /*this is the buffer to received the answer, and persist the answer until
     * the application wants it*/
    std::array<uint8_t, 128> buffer;

    hmp_sixbell::Response res;
};

HMPClient::HMPClient(const string ip, const int port)
    : port_(port), ip_(ip), socket_(service_) {}

void HMPClient::on_read(const boost::system::error_code& error,
                        size_t bytesReceived) {
    cout << "[SIX::HMPClient][on_read] response received, bytes received: "
         << bytesReceived << endl;
    if (error) {
        cerr << "[SIX::HMPClient][on_read] ERROR: " << error.message() << endl;
        return;
    }

    cout << "bytes received: ";
    for (int i = 0; i < bytesReceived; ++i) {
        printf("[%02X] ", buffer[i]);
    }
    cout << endl;

    auto msg_size = decode_header(buffer);
    bool parsedResWorked = res.ParseFromArray(&buffer[HEADER_SIZE], msg_size);
    if (parsedResWorked) {
        cout << "[SIX::HMPClient][on_read] response type [" << res.type() << "]"
             << endl;
    }

    socket_.close();
}

void HMPClient::on_write(const boost::system::error_code& error) {
    cout << "[SIX::HMPClient][on_write] request sent" << endl;

    if (error) {
        cerr << "[SIX::HMPClient][on_write] ERROR: " << error.message() << endl;
        return;
    }

    cout << "[SIX::HMPClient][on_write] ready to receive something " << endl;
    socket_.async_read_some(
        boost::asio::buffer(buffer),
        [&](const boost::system::error_code& error, size_t bytesReceived) {
            on_read(error, bytesReceived);
        });
}

void HMPClient::on_connect(const boost::system::error_code& error) {
    cout << "[SIX::HMPClient][on_connect] connection established" << endl;

    if (error) {
        cerr << "[on_connect] ERROR: " << error.message() << endl;
        return;
    }

    /*Request factory*/
    auto req = hmp_sixbell::Request();
    req.set_type(hmp_sixbell::Request_RequestType_PLAY);
    /****************/

    /*request marshaling*/
    std::vector<uint8_t> writebuf;
    int msg_size = req.ByteSize();
    cout << "msg to send size [" << msg_size << "]" << endl;
    writebuf.resize(msg_size + HEADER_SIZE);
    encode_header(writebuf, msg_size);
    req.SerializeToArray(&writebuf[HEADER_SIZE], msg_size);
    /***********************/

    /***** SEND *****/
    async_write(socket_, boost::asio::buffer(writebuf),
                [&](const boost::system::error_code& error,
                    const size_t bytesTransferred) { on_write(error); });
}

int HMPClient::async_play() {
    cout << "[SIX::HMPClient][play] initializing playing" << endl;

    try {
        boost::asio::ip::tcp::endpoint endpoint(
            boost::asio::ip::address::from_string(ip_), port_);

        socket_.async_connect(
            endpoint,
            [&](const boost::system::error_code& error) { on_connect(error); });

    } catch (std::exception& e) {
        cerr << e.what() << endl;
    }
    return 0;
}

void HMPClient::wait4Event() {
    cout << "[SIX::HMPClient][wait4Event] waiting for everything to be done"
         << endl;

    service_.run();
}

/* move semantics, to return res???? let's assume move semantics, or copy
 ellision*/
hmp_sixbell::Response HMPClient::getEvent() const { return res; }

const int SERVER_PORT = 4444;
const string SERVER_IP = "127.0.0.1";

int main(int argc, char* argv[]) {
    HMPClient c(SERVER_IP, SERVER_PORT);

    c.async_play();

    std::thread t([&]() {
        c.wait4Event();
        auto res = c.getEvent();
        cout << "[Main] Event received from app: [" << res.type() << "]"
             << endl;
    });

    t.join();

    return 0;
}
