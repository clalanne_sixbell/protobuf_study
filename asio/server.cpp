

#include "Server.hpp"

#include <boost/asio.hpp>

#include <iostream>

using std::cout;
using std::endl;

int main(int argc, char* argv[]) {
    boost::asio::io_service io;
    Server s(io, 4444);

    io.run();

    return 0;
}

