#!/usr/bin/python
#
# client.py: simple testing client for the server. Suitable for
# usage from the python interactive prompt.
#

from __future__ import print_function

import sys
from socket import *
import struct
from hmp_sixbell_pb2 import Request, Response


def make_socket(port=4444):
    """ Create a socket on localhost and return it.
    """
    sockobj = socket(AF_INET, SOCK_STREAM)
    sockobj.connect(('localhost', port))
    return sockobj


def send_message(sock, message):
    """ Send a serialized message (protobuf Message interface)
        to a socket, prepended by its length packed in 4 bytes.
    """
    s = message.SerializeToString()
    packed_len = struct.pack('>i', len(s))

    hex_chars_s = map(hex, map(ord, s))
    print("payload: [" + str(hex_chars_s) + "]")

    hex_chars_packed_len = map(hex, map(ord, packed_len))
    print("lenght: [" + str(hex_chars_packed_len) + "]")

    packed_message = packed_len + s
    hex_chars_all = map(hex, map(ord, packed_message))
    print("total message(payload + lenght): [" + str(hex_chars_all) + "]")

    sock.send(packed_message)


def socket_read_n(sock, n):
    """ Read exactly n bytes from the socket.
        Raise RuntimeError if the connection closed before n bytes were read.
    """
    buf = ''
    while n > 0:
        data = sock.recv(n)
        if data == '':
            raise RuntimeError('unexpected connection close')
        buf += data
        n -= len(data)
    return buf
    

def get_response(sock):
    """ Read a serialized response message from a socket.
    """
    msg = Response()
    len_buf = socket_read_n(sock, 4)
    msg_len = struct.unpack('>i', len_buf)[0]
    msg_buf = socket_read_n(sock, msg_len)
    msg.ParseFromString(msg_buf)
    return msg


def send_play(sock):
    rq = Request()
    rq.type = Request.PLAY
    print("[send_play] sending request type ["+ str(rq.type) + "]")
    print("[send_play] request ["+ str(rq) + "]")

    send_message(sock, rq)
    return get_response(sock)



if __name__ == '__main__':
    port = 4444
    if len(sys.argv) >= 2:
        port = int(sys.argv[1])

    sockobj = make_socket(port)

    response = send_play(sockobj) 

    print("response type: " + str(response.type))
