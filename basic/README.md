# protobuf_study

Everything regarding studying and prototyping protobuf!. The ```address_book``` 
should be a ```*.txt``` file

 * Always use ```GOOGLE_PROTOBUF_VERSION```

# Compilation *.proto

## OSX

```
protoc -I. --cpp_out=. addressbook.proto
```

# Compilation Programs

## OSX

```
clang++ insert.cpp addressbook.pb.cc -lprotobuf -o insert
```
```
clang++ read.cpp addressbook.pb.cc -lprotobuf -o read
```

# Execution

## insert
```
./insert address_book.txt
```

## read
```
./read address_book.txt
```
